# Q Tokenomics Telegram Bot

Delegation Bot for the Q Blockchain.

## Technologies used
- Node.js
- TypeScript
- Telegraf
- Web3

## Project structure

- `/src` – Main source folder
  - `/constants` – Constants & enums
  - `/handlers` – Bot commands & actions
  - `/helpers` – Contract & message helper functions
  - `/services` – Web3, I18n, WebSocket service providers
  - `/types` – TypeScript types
  - `/utils` – Formatters & util functions

## Environment

To run the bot you need to create `.env` file inside the project directory.

`.env` file structure:
```bash
# System contract registry address.
# More details: https://docs.q.org/dapp-dashboard/#blockchain
CONTRACT_REGISTRY_ADDRESS=0xc3E589056Ece16BCB88c6f9318e9a7343b663522
# Telegram Bot Token.
# Generate one using Bot Father Telegram Bot: https://t.me/BotFather
TELEGRAM_TOKEN=<BOT_FATHER_TOKEN>
# Q Blockchain RPC URL
RPC_URL=https://rpc.q.org
# Q Blockchain DApp URL
DAPP_URL=hq.q.org
# MongoDB connection string
DB_CONNECTION_STRING=mongodb://127.0.0.1:27017/db?directConnection=true&serverSelectionTimeoutMS=2000
```

## Local development

### Install dependencies
```bash
yarn install
```

### Run in development mode
```bash
yarn dev
```

### Build for production
```bash
yarn build
```

### Build & Run in production mode 
```bash
yarn start
```
