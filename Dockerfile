FROM node:16.10.0

RUN mkdir -p /app && chown -R node:node /app
WORKDIR /app
USER node

COPY --chown=node:node package.json yarn.lock .npmrc tsconfig.json ./
ARG NPM_TOKEN
RUN yarn config set '//gitlab.com/api/v4/packages/npm/:_authToken' $NPM_TOKEN
RUN yarn install --frozen-lockfile

COPY --chown=node:node src/ src/
RUN yarn build

CMD ["yarn", "start"]
