
import { formatAsset, formatPercent } from '@q-dev/utils';

import { findUser } from '@/models/User';

import { Q_VAULT_PATH } from '@/constants/urls';
import { I18nContext } from '@/types/telegraf';

import { captureError } from '@/services/error-handler';

import { getBackToMenuKeyboard } from '@/helpers/keyboards';
import { composeMessageLinks, replyWithHTML, replyWithMenuLink } from '@/helpers/messages';
import { getRewardDetails } from '@/helpers/q-vault';
import { formatDate } from '@/utils/formatters';

export async function handleQVaultBalance (ctx: I18nContext) {
  try {
    const user = await findUser(ctx.chatId);
    if (!user?.address) {
      replyWithMenuLink(ctx, ctx.i18n.t('NO_ADDRESS'));
      return;
    }

    const rewardDetails = await getRewardDetails(user.address);
    replyWithHTML(ctx, [
      [ctx.i18n.t('Q_TOKEN_HOLDER_REWARDS_TITLE'), composeMessageLinks(ctx, Q_VAULT_PATH)].join('\n'),
      ctx.i18n.t('Q_VAULT_BALANCE', {
        balance: formatAsset(rewardDetails.balance, 'Q')
      }),
      ctx.i18n.t('Q_REWARD_RATE', {
        rate: formatPercent(rewardDetails.interestRatePercentage)
      }),
      ctx.i18n.t('YEARLY_EXPECTED_REWARDS', {
        rewards: formatAsset(rewardDetails.yearlyExpectedEarnings, 'Q')
      }),
      ctx.i18n.t('REWARDS_ALLOCATED', {
        date: formatDate(rewardDetails.lastUpdate, user.language)
      }),
    ].join('\n\n'), {
      reply_markup: { inline_keyboard: getBackToMenuKeyboard(ctx) },
    });
  } catch (error) {
    captureError(error);
    replyWithMenuLink(ctx, ctx.i18n.t('UNKNOWN_ERROR_MSG'));
  }
}
