
import { I18nContext } from '@/types/telegraf';

import { getMainMenuKeyboard } from '@/helpers/keyboards';
import { replyWithHTML, replyWithMenuLink } from '@/helpers/messages';

export async function handleStart (ctx: I18nContext) {
  replyWithHTML(ctx, [ctx.i18n.t('WELCOME_MSG'), ctx.i18n.t('MENU_DESCRIPTION')].join('\n\n'), {
    reply_markup: { inline_keyboard: getMainMenuKeyboard(ctx) },
  });
}

export async function handleMenu (ctx: I18nContext) {
  replyWithHTML(
    ctx,
    [ctx.i18n.t('MENU_TITLE'), ctx.i18n.t('MENU_DESCRIPTION')].join('\n\n'),
    { reply_markup: { inline_keyboard: getMainMenuKeyboard(ctx) } }
  );
}

export function handleHelp (ctx: I18nContext) {
  replyWithMenuLink(ctx, [
    ctx.i18n.t('HELP_TITLE'),
    ctx.i18n.t('HELP_REWARDS_TOPIC'),
    ctx.i18n.t('HELP_DELEGATIONS_TOPIC'),
    ctx.i18n.t('HELP_ACCOUNT_TOPIC'),
  ].join('\n\n'));
}
