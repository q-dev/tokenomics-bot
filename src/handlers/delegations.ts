
import { formatAsset } from '@q-dev/utils';

import { findUser, updateUser } from '@/models/User';

import { DELEGATIONS_PATH, TOKENOMICS_PATH } from '@/constants/urls';
import { I18nContext } from '@/types/telegraf';

import { captureError } from '@/services/error-handler';

import { getDelegationsKeyboard } from '@/helpers/keyboards';
import { composeMessageLinks, replyWithHTML, replyWithMenuLink } from '@/helpers/messages';
import { getDelegationDetails } from '@/helpers/q-vault';
import { composeHqURL } from '@/helpers/urls';
import { trimAddress } from '@/utils/strings';

export async function handleDelegations (ctx: I18nContext) {
  try {
    const user = await findUser(ctx.chatId);
    if (!user?.address) {
      replyWithMenuLink(ctx, ctx.i18n.t('NO_ADDRESS'));
      return;
    }

    const {
      delegations,
      totalReward,
      totalDelegatedStake,
      defaultProxyBalance,
      validationProxyBalance
    } = await getDelegationDetails(user.address);

    const delegationItems = delegations.map((item, i) => {
      const validatorLink = composeHqURL(`/staking/validators/${item.validator}`);
      return ctx.i18n.t('DELEGATION_ITEM', {
        index: i + 1,
        address: `<a href="${validatorLink}">${trimAddress(item.validator)}</a>`,
        stake: formatAsset(item.actualStake, 'Q'),
        interpolation: { escapeValue: false }
      });
    });

    const delegationsMessageParts = [
      [ctx.i18n.t('DELEGATIONS_TITLE'), composeMessageLinks(ctx, DELEGATIONS_PATH)].join('\n'),
      [
        ctx.i18n.t('DELEGATIONS_LIST'),
        delegationItems.join('\n') || ctx.i18n.t('NO_DELEGATIONS')
      ].join('\n'),
      ctx.i18n.t('TOTAL_DELEGATED_STAKE', {
        stake: formatAsset(totalDelegatedStake, 'Q')
      }),
      ctx.i18n.t('OUTSTANDING_CLAIMABLE_REWARDS', {
        rewards: formatAsset(totalReward, 'Q')
      }),
    ];

    const proxyMessageParts = [
      [ctx.i18n.t('PROXIES_TITLE'), composeMessageLinks(ctx, TOKENOMICS_PATH)].join('\n'),
      ctx.i18n.t('DEFAULT_ALLOCATION_PROXY', {
        balance: formatAsset(defaultProxyBalance, 'Q')
      }),
      ctx.i18n.t('VALIDATION_REWARD_PROXY', {
        balance: formatAsset(validationProxyBalance, 'Q')
      }),
    ];

    replyWithHTML(ctx, [
      ...delegationsMessageParts,
      ...proxyMessageParts,
      ctx.i18n.t('NOTIFICATIONS_DESCRIPTION'),
    ].join('\n\n'), {
      reply_markup: { inline_keyboard: getDelegationsKeyboard(ctx, user) },
    });
  } catch (error) {
    captureError(error);
    replyWithMenuLink(ctx, ctx.i18n.t('UNKNOWN_ERROR_MSG'));
  }
}

export async function handleDelegationNotifications (ctx: I18nContext) {
  try {
    const user = await findUser(ctx.chatId);
    if (!user?.address) {
      replyWithMenuLink(ctx, ctx.i18n.t('NO_ADDRESS'));
      return;
    }

    await updateUser(ctx.chatId, { notifications: !user.notifications });
    replyWithMenuLink(ctx, user.notifications
      ? ctx.i18n.t('NOTIFICATIONS_DISABLED')
      : ctx.i18n.t('NOTIFICATIONS_ENABLED')
    );
  } catch (error) {
    captureError(error);
    replyWithMenuLink(ctx, ctx.i18n.t('UNKNOWN_ERROR_MSG'));
  }
}
