import { Message } from 'telegraf/typings/core/types/typegram';

import { createOrUpdateUser , createUser, deleteUser, findUser, updateUserLanguage } from '@/models/User';

import { I18nContext , Language } from '@/types/telegraf';

import { captureError } from '@/services/error-handler';
import { getI18nContext } from '@/services/i18n';
import { web3 } from '@/services/web3';

import { getAccountKeyboard, getBackToMenuKeyboard , getLanguagesKeyboard } from '@/helpers/keyboards';
import { replyWithHTML, replyWithMenuLink } from '@/helpers/messages';
import { trimAddress } from '@/utils/strings';

export async function handleAccount (ctx: I18nContext) {
  try {
    const user = await findUser(ctx.chatId);
    replyWithHTML(ctx, [
      ctx.i18n.t('ACCOUNT_TITLE'),
      ctx.i18n.t('YOUR_ADDRESS', {
        address: user?.address ? trimAddress(user?.address) : ctx.i18n.t('NOT_SET'),
      }),
      ctx.i18n.t('ACCOUNT_DESCRIPTION')
    ].join('\n\n'), {
      reply_markup: { inline_keyboard: getAccountKeyboard(ctx) },
    });
  } catch (error) {
    captureError(error);
    replyWithMenuLink(ctx, ctx.i18n.t('UNKNOWN_ERROR_MSG'));
  }
}

export async function handleAddress (ctx: I18nContext) {
  try {
    replyWithMenuLink(ctx, ctx.i18n.t('UPDATE_ADDRESS_DESCRIPTION'));
  } catch (error) {
    captureError(error);
    replyWithMenuLink(ctx, ctx.i18n.t('UNKNOWN_ERROR_MSG'));
  }
}

export async function handleUpdateAddress (ctx: I18nContext) {
  try {
    const text = (ctx.message as Message.TextMessage).text;
    if (web3.utils.isAddress(text)) {
      await createOrUpdateUser({
        _id: ctx.chatId,
        address: text,
        reward: 0,
        language: 'en',
        notifications: false,
      });
      replyWithHTML(ctx, ctx.i18n.t('ADDRESS_UPDATED'), {
        reply_markup: { inline_keyboard: getBackToMenuKeyboard(ctx) },
      });
      return;
    }

    replyWithMenuLink(ctx, ctx.i18n.t('ADDRESS_VALIDATION_ERROR'));
  } catch (error) {
    captureError(error);
    replyWithMenuLink(ctx, ctx.i18n.t('UNKNOWN_ERROR_MSG'));
  }
}

export function handleLanguage (ctx: I18nContext) {
  replyWithHTML(ctx, ctx.i18n.t('LANGUAGE_DESCRIPTION'), {
    reply_markup: { inline_keyboard: getLanguagesKeyboard(ctx) },
  });
}

export async function handleChangeLanguage (ctx: I18nContext, lang: Language) {
  try {
    const user = await findUser(ctx.chatId);
    if (!user) {
      await createUser({
        _id: ctx.chatId,
        address: '',
        language: 'en',
        reward: 0,
        notifications: false,
      });
    }

    await updateUserLanguage(ctx.chatId, lang);
    const i18nContext = await getI18nContext(ctx.chatId);
    ctx.i18n = i18nContext.i18n;
    replyWithMenuLink(ctx, ctx.i18n.t('LANGUAGE_CHANGED'));
  } catch (error) {
    captureError(error);
    replyWithHTML(ctx, ctx.i18n.t('UNKNOWN_ERROR_MSG'));
  }
}

export async function handleClearAccount (ctx: I18nContext) {
  try {
    const user = await findUser(ctx.chatId);
    if (user) {
      await deleteUser(ctx.chatId);
    }

    replyWithMenuLink(ctx, ctx.i18n.t('ACCOUNT_DELETED'));
  } catch (error) {
    captureError(error);
    replyWithMenuLink(ctx, ctx.i18n.t('UNKNOWN_ERROR_MSG'));
  }
}
