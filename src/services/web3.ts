import { ContractRegistryInstance } from '@q-dev/q-js-sdk';
import Web3 from 'web3';

import { config } from '@/config';
import { supportedContractTypes } from '@/constants/contracts';
import { ContractType, ContractValue } from '@/types/contracts';

export const web3 = new Web3(
  new Web3.providers.HttpProvider(config.RPC_URL)
);

export const contractRegistryInstance = new ContractRegistryInstance(web3, config.CONTRACT_REGISTRY_ADDRESS);
const cache: Record<string, ContractValue<any>> = {};

export function getContractInstance<T extends ContractType> (
  instance: T
): ContractValue<T> {
  if (!cache[instance]) {
    cache[instance] = contractRegistryInstance[instance]();
  }

  return cache[instance];
}

// Lazy load the contract instances
export function initContractInstances () {
  supportedContractTypes.forEach(getContractInstance);
}
