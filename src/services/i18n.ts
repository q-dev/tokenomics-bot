import i18next from 'i18next';

import { findUser } from '@/models/User';

import { I18nContext } from '@/types/telegraf';

import en from '@/locales/en.json';
// TODO: Enable when German translations added
// import de from '@/locales/de.json';
import uk from '@/locales/uk.json';

export async function initI18n () {
  await i18next.init({
    lng: 'en',
    fallbackLng: 'en',
    resources: {
      en: { translation: en },
      // TODO: Enable when German translations added
      // de: { translation: de },
      uk: { translation: uk },
    }
  });
}

export async function getI18nContext (id: number) {
  const user = await findUser(id);
  const language = user?.language || 'en';

  return {
    language,
    chat: { id },
    i18n: { t: i18next.getFixedT(language) }
  } as I18nContext;
}
