import * as Sentry from '@sentry/node';

import { config } from '@/config';

export function initErrorHandler () {
  Sentry.init({
    dsn: config.SENTRY_DSN,
    tracesSampleRate: 1.0,
  });

  Sentry.setContext('tokenomics bot info', {
    RPC_URL: config.RPC_URL,
    DAPP_URL: config.DAPP_URL,
  });
}

export function captureError (error: any, ...args: any[]) {
  try {
    Sentry.captureException(error, {
      extra: { details: JSON.stringify(args) }
    });
    console.info(error);
  } catch (error) {
    console.error(error);
  }
}
