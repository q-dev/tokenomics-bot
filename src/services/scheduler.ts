import { formatAsset } from '@q-dev/utils';
import { IntervalBasedCronScheduler, parseCronExpression } from 'cron-schedule';

import { findAllUsers, IUser, updateUser } from '@/models/User';

import { captureError } from '@/services/error-handler';

import { getClaimKeyboard } from '@/helpers/keyboards';
import { sendMessageToChat } from '@/helpers/messages';
import { getTotalDelegationReward } from '@/helpers/q-vault';

import { getI18nContext } from './i18n';

// Check reports every 30 minutes
const cron = parseCronExpression('*/30 * * * * *');
// Check every minute
const scheduler = new IntervalBasedCronScheduler(5 * 1_000);

export async function initScheduler () {
  scheduler.registerTask(cron, async () => {
    try {
      const users = await findAllUsers();
      const usersToNotify = users.filter(user => user.address && user.notifications);
      await updateDelegationRewards(usersToNotify);
    } catch (error) {
      captureError(error);
    }
  });
}

async function updateDelegationRewards (users: IUser[]) {
  await Promise.all(
    users.map(async (user) => {
      const totalReward = await getTotalDelegationReward(user.address);
      const isRewardChanged = user.reward !== totalReward;

      const ctx = await getI18nContext(user._id);
      if (isRewardChanged && totalReward > 0) {
        sendMessageToChat(Number(user._id), ctx.i18n.t('DELEGATION_REWARD_UPDATE', {
          rewards: formatAsset(totalReward, 'Q'),
        }), { reply_markup: { inline_keyboard: getClaimKeyboard(ctx) } });
      }

      if (isRewardChanged) {
        await updateUser(user._id, { reward: totalReward });
      }
    })
  );
}
