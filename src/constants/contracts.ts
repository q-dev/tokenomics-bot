import { ContractType } from '@/types/contracts';

export const supportedContractTypes: ContractType[] = [
  'qVault',
  'defaultAllocationProxy',
  'validationRewardProxy'
];
