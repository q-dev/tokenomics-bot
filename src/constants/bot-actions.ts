export enum BotActions {
  START = 'start',
  HELP = 'help',
  MENU = 'menu',

  Q_REWARDS = 'q_rewards',
  DELEGATIONS = 'delegations',
  TOGGLE_NOTIFICATIONS = 'toggle_notifications',

  ACCOUNT = 'account',
  UPDATE_ADDRESS = 'update_address',
  CLEAR_ACCOUNT = 'clear_account',

  LANGUAGE = 'language',
  ENGLISH = 'english',
  GERMAN = 'german',
  UKRAINIAN = 'ukrainian',

  TEXT = 'text',
  CALLBACK_QUERY = 'callback_query',
}
