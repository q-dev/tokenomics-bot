export const Q_VAULT_PATH = '/q-vault';
export const DELEGATIONS_PATH = '/staking/delegations';
export const TOKENOMICS_PATH = '/dashboard/tokenomics';
