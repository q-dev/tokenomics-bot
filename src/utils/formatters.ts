import { format, Locale } from 'date-fns';
import de from 'date-fns/locale/de';
import en from 'date-fns/locale/en-GB';
import uk from 'date-fns/locale/uk';

import { Language } from '@/types/telegraf';

const dateLocales: Record<Language, Locale> = {
  en: en,
  de: de,
  uk: uk
};

export function formatDate (
  value: string | number | Date,
  language: Language = 'en',
  pattern = 'PPppp'
) {
  const date = new Date(value);
  if (!date || date.getTime() === 0) return '–';

  return format(date, pattern, { locale: dateLocales[language] });
}
