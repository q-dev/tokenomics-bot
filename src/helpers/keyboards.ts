import { InlineKeyboardButton } from 'telegraf/typings/core/types/typegram';

import { IUser } from '@/models/User';

import { BotActions } from '@/constants/bot-actions';
import { DELEGATIONS_PATH } from '@/constants/urls';
import { I18nContext } from '@/types/telegraf';

import { composeHqURL, composeMetamaskURL } from './urls';

export function getMainMenuKeyboard (ctx: I18nContext): InlineKeyboardButton[][] {
  return [
    [
      { text: ctx.i18n.t('Q_REWARDS'), callback_data: BotActions.Q_REWARDS },
      { text: ctx.i18n.t('DELEGATIONS'), callback_data: BotActions.DELEGATIONS }
    ],
    [
      { text: ctx.i18n.t('ACCOUNT'), callback_data: BotActions.ACCOUNT },
      { text: ctx.i18n.t('HELP'), callback_data: BotActions.HELP }
    ],
  ];
}

export function getDelegationsKeyboard (ctx: I18nContext, user: IUser): InlineKeyboardButton[][] {
  return [
    [{
      text: user.notifications
        ? ctx.i18n.t('REWARD_NOTIFICATIONS_ON')
        : ctx.i18n.t('REWARD_NOTIFICATIONS_OFF'),
      callback_data: BotActions.TOGGLE_NOTIFICATIONS
    }],
    [{ text: ctx.i18n.t('BACK_TO_MENU'), callback_data: BotActions.MENU }],
  ];
}

export function getAccountKeyboard (ctx: I18nContext): InlineKeyboardButton[][] {
  return [
    [
      { text: ctx.i18n.t('UPDATE_ADDRESS'), callback_data: BotActions.UPDATE_ADDRESS },
      { text: ctx.i18n.t('LANGUAGE'), callback_data: BotActions.LANGUAGE },
    ],
    [{ text: ctx.i18n.t('CLEAR_ACCOUNT_DATA'), callback_data: BotActions.CLEAR_ACCOUNT }],
    [{ text: ctx.i18n.t('BACK_TO_MENU'), callback_data: BotActions.MENU }],
  ];
}

export function getBackToMenuKeyboard (ctx: I18nContext): InlineKeyboardButton[][] {
  return [
    [{ text: ctx.i18n.t('BACK_TO_MENU'), callback_data: BotActions.MENU }],
  ];
}

export function getLanguagesKeyboard (ctx: I18nContext): InlineKeyboardButton[][] {
  return [
    [
      { text: ctx.i18n.t('ENGLISH'), callback_data: BotActions.ENGLISH },
      // TODO: Enable when German translations added
      // { text: ctx.i18n.t('GERMAN'), callback_data: BotActions.GERMAN },
      { text: ctx.i18n.t('UKRAINIAN'), callback_data: BotActions.UKRAINIAN },
    ],
    [{ text: ctx.i18n.t('BACK_TO_MENU'), callback_data: BotActions.MENU }],
  ];
}

export function getClaimKeyboard (ctx: I18nContext): InlineKeyboardButton[][] {
  return [
    [{ text: ctx.i18n.t('CLAIM_IN_YOUR_HQ'), url: composeHqURL(DELEGATIONS_PATH) }],
    [{ text: ctx.i18n.t('CLAIM_IN_METAMASK_BROWSER'), url: composeMetamaskURL(DELEGATIONS_PATH) }],
    [{ text: ctx.i18n.t('BACK_TO_MENU'), callback_data: BotActions.MENU }],
  ];
}
