import { config } from '@/config';

export function composeMetamaskURL (path: string) {
  const normalizedDappUrl = config.DAPP_URL.replace(/^https?:\/\//, '');
  return `https://metamask.app.link/dapp/${normalizedDappUrl}${path}`;
}

export function composeHqURL (path: string) {
  return config.DAPP_URL + path;
}
