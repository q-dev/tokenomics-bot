import { calculateInterestRate, unixToDate } from '@q-dev/utils';
import { sum } from 'lodash';

import { getContractInstance, web3 } from '@/services/web3';

export async function getRewardDetails (address: string) {
  const contract = await getContractInstance('qVault');
  const compoundRateKeeper = await contract.getCompoundRateKeeper();

  const [balance, balanceDetails, lastUpdate] = await Promise.all([
    contract.balanceOf(address),
    contract.getBalanceDetails(address),
    compoundRateKeeper.getLastUpdate(),
  ]);

  const interestRatePercentage = calculateInterestRate(Number(balanceDetails.interestRate));
  const vaultBalance = web3.utils.fromWei(balance);

  return {
    balance: vaultBalance,
    interestRatePercentage,
    lastUpdate: unixToDate(lastUpdate),
    yearlyExpectedEarnings: Number(vaultBalance) * interestRatePercentage / 100
  };
}

export async function getDelegationDetails (address: string) {
  const [qVault, defaultAllocationProxy, validationRewardProxy] = await Promise.all([
    getContractInstance('qVault'),
    getContractInstance('defaultAllocationProxy'),
    getContractInstance('validationRewardProxy')
  ]);

  const [delegations, defaultProxyBalance, validationProxyBalance] = await Promise.all([
    qVault.getDelegationsList(address),
    defaultAllocationProxy.getBalance(),
    validationRewardProxy.getBalance()
  ]);

  const delegationsList = delegations.map((delegation) => ({
    validator: delegation.validator,
    claimableReward: web3.utils.fromWei(delegation.claimableReward),
    actualStake: web3.utils.fromWei(delegation.actualStake),
  }));

  return {
    delegations: delegationsList,
    totalReward: sum(delegationsList.map((delegation) => Number(delegation.claimableReward))),
    totalDelegatedStake: sum(delegationsList.map((delegation) => Number(delegation.actualStake))),
    defaultProxyBalance,
    validationProxyBalance,
  };
}

export async function getTotalDelegationReward (address: string) {
  const qVault = await getContractInstance('qVault');
  const delegations = await qVault.getDelegationsList(address);

  return sum(delegations.map((delegation) => {
    return Number(web3.utils.fromWei(delegation.claimableReward));
  }));
}
