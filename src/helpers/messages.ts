import { Context } from 'telegraf';
import { ExtraReplyMessage } from 'telegraf/typings/telegram-types';

import { I18nContext } from '@/types/telegraf';

import { bot } from '@/app';
import { captureError } from '@/services/error-handler';

import { getBackToMenuKeyboard } from './keyboards';
import { composeHqURL, composeMetamaskURL } from './urls';

export async function sendMessageToChat (
  chatId: number,
  message: string,
  extra: ExtraReplyMessage = {}
) {
  try {
    await bot.telegram.sendMessage(chatId, message, {
      ...extra,
      parse_mode: 'HTML',
      disable_web_page_preview: true
    });
  } catch (error) {
    captureError(error);
  }
}

export function replyWithHTML (
  ctx: Context,
  message: string,
  extra: ExtraReplyMessage = {}
) {
  return ctx.reply(message, {
    ...extra,
    disable_web_page_preview: true,
    parse_mode: 'HTML',
  });
}

export function replyWithMenuLink (ctx: I18nContext, message: string) {
  return replyWithHTML(ctx, message, {
    reply_markup: { inline_keyboard: getBackToMenuKeyboard(ctx) },
  });
}

export function composeMessageLinks (ctx: I18nContext, path: string) {
  return [
    `<a href="${composeHqURL(path)}">${ctx.i18n.t('YOUR_HQ')}</a>`,
    `<a href="${composeMetamaskURL(path)}">${ctx.i18n.t('METAMASK_BROWSER')}</a>`
  ].join(' | ');
}
