import mongoose from 'mongoose';
import { Telegraf } from 'telegraf';

import 'module-alias/register';

import {
  handleAccount,
  handleAddress,
  handleChangeLanguage,
  handleClearAccount,
  handleLanguage,
  handleUpdateAddress
} from '@/handlers/account';
import { handleHelp, handleMenu, handleStart } from '@/handlers/common';
import { handleDelegationNotifications, handleDelegations } from '@/handlers/delegations';
import { handleQVaultBalance } from '@/handlers/rewards';

import { config } from '@/config';
import { BotActions } from '@/constants/bot-actions';
import { I18nContext } from '@/types/telegraf';

import { initErrorHandler } from '@/services/error-handler';
import { initI18n } from '@/services/i18n';
import { initScheduler } from '@/services/scheduler';
import { initContractInstances } from '@/services/web3';

import { useChat } from '@/middlewares/use-chat';
import { useI18n } from '@/middlewares/use-i18n';

export const bot = new Telegraf<I18nContext>(config.TELEGRAM_TOKEN);

async function init () {
  try {
    initErrorHandler();

    await mongoose.connect(config.DB_CONNECTION_STRING);
    console.info('Connected to DB');
    await initI18n();

    initContractInstances();
    initScheduler();

    bot.use(useChat);
    bot.use(useI18n);

    initBotHandlers();
    bot.launch();

    console.info('Bot started');
  } catch (error) {
    console.error('Failed to initialize bot', error);
  }
}

function initBotHandlers () {
  bot.command(BotActions.START, handleStart);
  bot.command(BotActions.HELP, handleHelp);

  bot.action(BotActions.MENU, handleMenu);
  bot.action(BotActions.HELP, handleHelp);

  bot.action(BotActions.Q_REWARDS, handleQVaultBalance);
  bot.action(BotActions.DELEGATIONS, handleDelegations);
  bot.action(BotActions.TOGGLE_NOTIFICATIONS, handleDelegationNotifications);

  bot.action(BotActions.ACCOUNT, handleAccount);
  bot.action(BotActions.UPDATE_ADDRESS, handleAddress);
  bot.action(BotActions.CLEAR_ACCOUNT, handleClearAccount);

  bot.action(BotActions.LANGUAGE, handleLanguage);
  bot.action(BotActions.ENGLISH, ctx => handleChangeLanguage(ctx, 'en'));
  // TODO: Enable when German translations added
  // bot.action(BotActions.GERMAN, ctx => handleChangeLanguage(ctx, 'de'));
  bot.action(BotActions.UKRAINIAN, ctx => handleChangeLanguage(ctx, 'uk'));

  bot.on(BotActions.TEXT, handleUpdateAddress);
}

init();

// Enable graceful stop
process.once('SIGINT', () => bot.stop('SIGINT'));
process.once('SIGTERM', () => bot.stop('SIGTERM'));
