import i18next from 'i18next';
import { Context } from 'telegraf';

import { ChatContext, I18nContext } from '@/types/telegraf';

export function useI18n (ctx: Context, next: () => Promise<any>) {
  (ctx as I18nContext).i18n = {
    t: i18next.getFixedT((ctx as ChatContext).language)
  };

  return next();
}
