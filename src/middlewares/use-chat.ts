import { Context } from 'telegraf';

import { findUser } from '@/models/User';

import { ChatContext } from '@/types/telegraf';

import { captureError } from '@/services/error-handler';

export async function useChat (ctx: Context, next: () => Promise<any>) {
  if (!ctx.chat?.id) return;

  try {
    const user = await findUser(ctx.chat.id);
    (ctx as ChatContext).language = user?.language || 'en';
    (ctx as ChatContext).chatId = ctx.chat.id;
  } catch (e) {
    captureError(e);
    return;
  }

  return next();
}
