import mongoose from 'mongoose';

import { Language } from '@/types/telegraf';

export interface IUser {
  _id: number;
  address: string;
  reward: number;
  language: Language;
  notifications: boolean;
}

export const UserSchema = new mongoose.Schema({
  _id: { type: Number, required: true },
  address: { type: String, required: true },
  reward: { type: Number, default: 0 },
  language: { type: String, default: 'en' },
  notifications: { type: Boolean, default: false },
}, { collection: 'User' });

const User = mongoose.model<IUser>('User', UserSchema);
export default User;

export async function createUser (user: IUser) {
  const newUser = new User(user);
  await newUser.save();
  return newUser;
}

export async function findUser (id: number) {
  return User.findOne({ _id: id });
}

export async function findAllUsers () {
  return User.find();
}

export async function updateUser (id: number, user: Partial<IUser>) {
  return User.updateOne({ _id: id, ...user });
}

export async function deleteUser (id: number) {
  await User.deleteOne({ _id: id });
}

export async function createOrUpdateUser (newUser: IUser) {
  const user = await findUser(newUser._id);
  return user
    ? updateUser(newUser._id, newUser)
    : createUser(newUser);
}

export async function updateUserLanguage (id: number, language: Language) {
  await User.updateOne({ _id: id }, { language });
}
