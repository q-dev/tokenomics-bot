import 'dotenv/config';

export const config = {
  TELEGRAM_TOKEN: String(process.env.TELEGRAM_TOKEN || ''),
  CONTRACT_REGISTRY_ADDRESS: String(process.env.CONTRACT_REGISTRY_ADDRESS || ''),
  RPC_URL: String(process.env.RPC_URL || ''),
  DAPP_URL: String(process.env.DAPP_URL || ''),
  DB_CONNECTION_STRING: String(process.env.DB_CONNECTION_STRING || ''),
  SENTRY_DSN: String(process.env.SENTRY_DSN || ''),
};
