import { Context } from 'telegraf';
import { Update } from 'telegraf/typings/core/types/typegram';
import { Deunionize } from 'telegraf/typings/deunionize';

export type Language = 'en' | 'de' | 'uk';

export interface ChatContext<U extends Deunionize<Update> = Update> extends Context<U> {
  chatId: number;
  language: Language;
}

export interface I18nContext<U extends Deunionize<Update> = Update> extends ChatContext<U> {
  i18n: {
    t: (key: string, options?: any) => string;
  };
}
