import { ContractRegistryInstance } from '@q-dev/q-js-sdk';
import { BaseContractInstance } from '@q-dev/q-js-sdk/lib/contracts/BaseContractInstance';

type KeyOfType<T, U> = {
  [P in keyof T]: T[P] extends U ? P: never
}[keyof T];

type ContractPromise = Promise<BaseContractInstance<any>>;
export type ContractType = KeyOfType<ContractRegistryInstance, () => ContractPromise>;
export type ContractValue<T extends ContractType> = ReturnType<ContractRegistryInstance[T]>;
